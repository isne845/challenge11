#include <iostream>
using namespace std;

template <class T>

void swap_values(T& variable1, T& variable2)
{
T temp;
temp = variable1;
variable1 = variable2;
variable2 = temp;
}



template <class T>
int index_of_smallest(const T a[], int start_index, int number_used)
{
	
	T min = a[start_index];
	int index_of_min = start_index;
		for (int index = start_index + 1; index < number_used; index++)
			if (a[index] < min)
			{
				min = a[index];
				index_of_min = index;
			}
					return index_of_min;
}
template <class T>
	void sort(T a[], int number_used)
		{
		int index_of_next_smallest;
			for (int index = 0; index < number_used - 1; index++)
			{
		index_of_next_smallest = index_of_smallest(a, index, number_used);
		swap_values(a[index], a[index_of_next_smallest]);
			}
		}

int main(){
	int a[5]={4,2,3,5,1};
	cout << "Before sorting Number :";
	for(int i=0;i<5;i++){
		cout << a[i] << " ";
			}
cout << endl;
			sort(a,5);
	cout << "Sorting Number :";
	for(int i=0;i<5;i++){
		cout << a[i] << " ";
	}
	cout << endl;
	
	char b[] = {'a','b','d','c','e'};
	cout << "Before sorting Character :";
	for (int i=0;i<5;i++){
		cout << b[i]<< " ";
	}
	cout << endl;
	cout << "Sorting Character :";
	sort(b,5);
	for(int i=0;i<5;i++){
		cout << b[i] << " ";
	}
	cout << endl;
	double c[] = {1.25,5.25,4.25,3.25,2.25};
	cout << "Before sorting Double :";
	for(int i=0;i<5;i++){
		cout << c[i] << " ";
	}
	cout << endl;
	
	cout << "Sorting Double :";
	sort(c,5);
	for(int i=0;i<5;i++){
		cout << c[i] << " ";
	}
	cout << endl;
	}



